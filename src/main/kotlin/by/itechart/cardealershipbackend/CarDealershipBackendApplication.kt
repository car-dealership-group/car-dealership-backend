package by.itechart.cardealershipbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CarDealershipBackendApplication

fun main(args: Array<String>) {
    runApplication<CarDealershipBackendApplication>(*args)
}

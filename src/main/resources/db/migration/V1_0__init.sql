CREATE TABLE base_vehicles
(
    id           UUID             NOT NULL
        CONSTRAINT base_vehicles_pkey
            PRIMARY KEY,
    created_at   TIMESTAMP,
    deleted_at   TIMESTAMP,
    updated_at   TIMESTAMP,
    color        VARCHAR(255)     NOT NULL,
    manufacturer VARCHAR(255)     NOT NULL,
    model        VARCHAR(255)     NOT NULL,
    package_type VARCHAR(255)     NOT NULL,
    price        DOUBLE PRECISION NOT NULL
);

CREATE TABLE users
(
    id         UUID         NOT NULL
        CONSTRAINT users_pkey
            PRIMARY KEY,
    created_at TIMESTAMP,
    deleted_at TIMESTAMP,
    updated_at TIMESTAMP,
    email      VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    password   VARCHAR(255) NOT NULL,
    patronymic VARCHAR(255),
    phone      VARCHAR(255) NOT NULL,
    role       VARCHAR(50)  NOT NULL DEFAULT 'CUSTOMER'
);

CREATE TABLE customers
(
    extras VARCHAR(255),
    id     UUID NOT NULL
        CONSTRAINT customers_pkey
            PRIMARY KEY
        CONSTRAINT fk_users_on_customers
            REFERENCES users
);

CREATE TABLE managers
(
    id UUID NOT NULL
        CONSTRAINT managers_pkey
            PRIMARY KEY
        CONSTRAINT fk_users_on_managers
            REFERENCES users
);

CREATE TABLE contracts
(
    id            UUID             NOT NULL
        CONSTRAINT contracts_pkey
            PRIMARY KEY,
    created_at    TIMESTAMP,
    deleted_at    TIMESTAMP,
    updated_at    TIMESTAMP,
    payed_at      DATE,
    signed_at     DATE,
    terminated_at DATE,
    total_price   DOUBLE PRECISION NOT NULL,
    customer_id   UUID
        CONSTRAINT fk_customers_on_contracts
            REFERENCES customers,
    manager_id    UUID
        CONSTRAINT fk_mangers_on_contracts
            REFERENCES managers
);

CREATE TABLE vehicles
(
    vin             VARCHAR(17)      NOT NULL,
    id              UUID             NOT NULL
        CONSTRAINT vehicles_pkey
            PRIMARY KEY,
    created_at      TIMESTAMP,
    deleted_at      TIMESTAMP,
    updated_at      TIMESTAMP,
    arrived_at      TIMESTAMP,
    issued_at       INTEGER          NOT NULL,
    price           DOUBLE PRECISION NOT NULL,
    reserved_at     TIMESTAMP,
    sold_at         TIMESTAMP,
    base_vehicle_id UUID
        CONSTRAINT fk_base_vehicles_on_vehicles
            REFERENCES base_vehicles,
    contract_id     UUID
        CONSTRAINT fk_contracts_on_vehicles
            REFERENCES contracts
);
